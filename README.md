# Madbot documentation

[![Built with Material for MkDocs](https://img.shields.io/badge/Material_for_MkDocs-526CFE?style=for-the-badge&logo=MaterialForMkDocs&logoColor=white)](https://squidfunk.github.io/mkdocs-material/)

## Foreword

Our documentation is available [here](https://ifb-elixirfr.gitlab.io/madbot/madbot-doc/).

## Project layout

```text
mkdoc_env.yml           # Conda env to build and test the site locally
README.md               # General readme 
mkdocs.yml              # The configuration file.
CONTRIBUTING.md         # Contributing file
code_of_conduct.md      # Code of conduct file
docs/
    index.md  # The documentation homepage.
    ...       # Other markdown pages, images and other files.
```

## For collaborators and developers

This part is for collaborators and developers.

### Modify content

When you are in the repository, add and/or modify your markdown files in the docs directory. The arborescence of the website menu is to setup in the mkdocs.yml file.

### Mkdocs

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. Create and activate conda env

```bash
conda env create -f mkdoc_env.yml
conda activate mkdoc_env
```

3. Preview your project

```bash
mkdocs serve
# Or if the port is already used 
mkdocs serve -a localhost:8400  
```

Note : *The site can be accessed under <http://localhost:8400/>

4. Add content

Read more at MkDocs [documentation](https://www.mkdocs.org/).

5. Try to build

```bash
mkdocs build
```

The website is built in a `site/` folder.
This folder is ignored (and must be ignored) by git.

6. Update the repo

The CI takes care of the rest

## Citation

If you use our guide, please cite us :

IFB-ElixirFr, Madbot documentation, (2023), GitLab repository, <https://gitlab.com/ifb-elixirfr/madbot/madbot-doc>

A DOI with Zenodo is comming.

## Contributors

* [Bouri Laurent](https://gitlab.com/lbouri1) <a itemprop="sameAs" content="https://orcid.org/0000-0002-2297-1559" href="https://orcid.org/0000-0002-2297-1559" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* [Denecker Thomas](https://gitlab.com/thomasdenecker) <a itemprop="sameAs" content="https://orcid.org/0000-0003-1421-7641" href="https://orcid.org/0000-0003-1421-7641" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* [Messak Imane](https://gitlab.com/imanemessak) <a itemprop="sameAs" content="https://orcid.org/0000-0002-1654-6652" href="https://orcid.org/0000-0002-1654-6652" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* [Mohamed Anliat](https://gitlab.com/anliatm) <a itemprop="sameAs" content="https://orcid.org/0000-0002-1105-8262" href="https://orcid.org/0000-0002-1105-8262" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* [Rousseau Baptiste](https://gitlab.com/nobabar) <a itemprop="sameAs" content="https://orcid.org/0009-0002-1723-2732" href="https://orcid.org/0009-0002-1723-2732" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* [Seiler Julien](https://gitlab.com/julozi) <a itemprop="sameAs" content="https://orcid.org/0000-0002-4549-5188" href="https://orcid.org/0000-0002-4549-5188" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>

## Contributing

Please, see the [CONTRIBUTING](CONTRIBUTING.md) file.

## Contributor Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](https://www.contributor-covenant.org/). By participating in this project you agree to abide by its terms. See [CODE_OF_CONDUCT](code_of_conduct.md) file.

## License

[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Madbot API is licensed under a [The 3-Clause BSD License License](https://opensource.org/license/bsd-3-clause/).

## Acknowledgement

* Nadia GOUE and Matéo HIRIART for their contribution to openLink ;
* The working group for its advisory role ;
* All application testers.

## Ressources

### mkdocs

* Official documentation : <https://www.mkdocs.org/>
* Material (best extension !) : <https://squidfunk.github.io/mkdocs-material/>
* Multi language structure : <https://github.com/squidfunk/mkdocs-material/discussions/2346>

### Commit

* <https://blog.stack-labs.com/code/devops_conventional_changelog/>
* <https://www.conventionalcommits.org/en/v1.0.0/>
