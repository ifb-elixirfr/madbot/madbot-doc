Madbot is a web application offering a dashboard for managing research data.

![Madbot](images/madbot_ui.png)

Designed around a set of connectors, Madbot interfaces with numerous data management and manipulation tools used daily by researchers, such as Omero, LabGuru, SFTP, Galaxy, Seafile, Zenodo and many others. It offers a cross-disciplinary view of data for each research project, while helping scientists to adopt the FAIR principles and publish their data. It is based on recognized standards and conventions such as the ISA model.

## Key concepts

Madbot is using some simple concepts to help you describe reasearch projects.

### ISA objects

When you describe a research project in Madbot we invite you to use what we call **ISA objectcs**.

ISA for **I**nvestigation, **S**tudy and **A**ssay is an standard hierarchical model used to describe research project.

Following the definitions of the ISA model **Investigations** represent the main objectives of a project, **Studies** are used to describe partiuclar biological hypothesis that you plan to test in different ways and **Assays** represent experiments, measurements or models.

Even if the [official ISA documentation](https://isa-specs.readthedocs.io/en/latest/isamodel.html) gives detailed informations of what should be an each type of object, Madbot tries to keep an open-minded approeach and let you use each type as you wish. The only rules are that an Assay must be contained in a Study and a Study must be contained in an Investigation.

### Data objects

Madbot doesn't store any huge scientific data: that's not its role. It does, however, enable you to identify exactly where the data associated with your research is located.

For Madbot, each piece of data is called a data object. It could be a folder on your lab's network storage, a set of images on your Omero server or even a simple file you've identified in a Zenodo publication.

### Tools

Tools are the entities where the data objects of interest are stored. In the context of Madbot, these tools might be databases, repositories, or other storage systems.

### DataLinks

DataLinks represent the links or images of the data of interest. Madbot stores only metadata, meaning that the actual data is represented only by its DataLink. To create a DataLink, a user must select a DataObject (which represents the actual data) within the interface.

With these few concepts in mind, you can now [create your first investigation](User_guide.md).
