# Your first investigation...

When you start using Madbot for the first time, you will be guided to create your first investigation

![Your first investigation](images/first_investigation.png)

Choose a name for your first investigation and click on the "START" button.

You can either import an investigation from a tool or click on the "empty investigation" button to start from a blank investigation.

![create_investigation](images/create_investigation.png)

!!! info ""
    You can rename an ISA object and modify its description just by clicking on its title or description. Any modification will be saved automatically.

### Deleting an ISA Object

**Step 1**: Select the ISA object you wish to delete
**Step 2**: Click on the three dots button on right top of the page to delete the ISA object
**Step 3**: For confirmation, type in the name of the ISA object you're removing.

![delete_isa](images/delete_isa.png)

![confirm_delete](images/confirm_delete.png)

!!! warning
    Be careful when deleting an ISA object. This action is irreversible.

## Managing DataLinks

### Adding a DataLink

**Step 1**: Go to the ISA object you want to add a DL, and click on the button below "new link" to choose the connector where you want to get the DL

![new_link](images/new_link.png)

**Step 2**: Choose the appropriate connector from the list from which the DL will be sourced and click on "CREATE DATALINK"

![chosse_tool](images/choose_tool.png)

**Step 3**:  Fill in the necessary fields, according to the connector, as shown below, and then click on the "next" button:

![fill_DL](images/fill_DL.png)

**Step 4**: Navigate through files and directories within the interface to locate the DataObject of interest

![browse_DL](images/browse_data.png)

**Step 5**: Once your desired DataObject is selected, click on "CREATE DATALINK".

**Step 6**: The new DataLink has now been integrated with the specified ISA object. You should be able to view or access it from the interface

![see_DL](images/see_DL.png)

### Deleting a DataLink

*Coming soon*

---

We appreciate your commitment to Madbot. This guide aims to simplify your experience and help you realise the full potential of the application. If you have any further questions or need assistance, please do not hesitate to contact our team.
