
# I Have a Question

> If you want to ask a question, we assume that you have read the available [Documentation](https://madbot-doc-ifb-elixirfr-madbot-862236cdf1294be68fb99caa61bebfec.gitlab.io/).

Before you ask a question, it is best to search for existing issues that might help you. 

- [API Issues](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/-/issues)
- [Client Issues](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/-/issues)
- [Doc Issues](https://gitlab.com/ifb-elixirfr/madbot/madbot-doc/-/issues)
- [Portal Issues](https://gitlab.com/ifb-elixirfr/madbot/madbot-portal/-/issues)

In case you have found a suitable issue and still need clarification, you can write your question in this issue. It is also advisable to search the internet for answers first.

If you then still feel the need to ask a question and need clarification, we recommend the following:

- Open an issue:
  - [API Issue](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/-/issues/new)
  - [Client Issue](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/-/issues/new)
  - [Doc Issue](https://gitlab.com/ifb-elixirfr/madbot/madbot-doc/-/issues/new)
  - [Portal Issue](https://gitlab.com/ifb-elixirfr/madbot/madbot-portal/-/issues/new)
- Provide as much context as you can about what you're running into.
- Provide project and platform versions (nodejs, npm, etc), depending on what seems relevant.

We will then take care of the issue as soon as possible.