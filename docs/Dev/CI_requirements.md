# Continuous Integration (CI)

## Introduction - CI objectives

Continuous integration (CI) is a software development practice that involves automating the process of preparing an application for deployment, through the execution of the build and test processes. This makes it possible to detect errors quickly and guarantee code quality and integrity with each modification.

## API - CI stages

### 1. Code quality - Ruff

[Ruff](https://docs.astral.sh/ruff/) is a particularly fast linter and formatter for python. It is used to ensure code quality and compliance with style and formatting standards. It will ensure that code is consistent and easy to read, facilitating collaboration between developers. The target python version is 3.10.

```bash
pip install ruff
```

#### Linter

```bash
ruff check . --select I
```

Ruff's linter uses the rules of various other linters; we'll be using [Pyflakes](https://pypi.org/project/pyflakes/)' rules as well as some [Pycodestyle](https://pycodestyle.pycqa.org/en/latest/intro.html) rules (E4, E7, E9).

#### Formatter

```bash
ruff format --check .
```

Ruff's trainer uses a configuration very similar to that of [Black](https://pypi.org/project/black/) in order to be as compatible as possible. Thus, we will use the same settings as Black:

- maximum line length of 88 characters
- indentation width of 4 spaces
- indentation composed of spaces rather than tabs
- use of double quotation marks for strings

### 2. Dependency vulnerability - Safety

[Safety](https://pyup.io/safety/) is a security scanner for Python dependencies. It helps us identify known vulnerabilities in the Python packages used by the project and take the necessary steps to correct them.

```bash
pip install safety
safety check
```

### 3. Security vulnerabilities - Bandit

[Bandit](https://bandit.readthedocs.io/en/latest/) is a security tool for Python that scans source code for potential security problems. It is used to identify vulnerabilities and risky coding practices.

We use bandit's default settings, with the exception of the B507 plugin (`ssh_no_host_key_verification`), which we don't use. We also exclude the test directory.

```bash
pip install "bandit[toml]"
bandit -c pyproject.toml -r madbot_api
```

### 4. Testing - Unittest

Our CI also includes functional and unit tests. Functional testing verifies that the application as a whole functions correctly, and that it meets the functional specifications. Unit tests, on the other hand, check the correct operation of isolated parts of the application. Tests are written with Django Test ([unittest](https://docs.python.org/3/library/unittest.html)).

```bash
python manage.py test
```

One metric we use to guarantee test efficiency is code coverage. We use [Coverage.py](https://coverage.readthedocs.io/en/coverage-5.5/) to measure the code coverage of our tests. We're aiming for 100% coverage for Madbot core.

!!! warning
    Code coverage is not a perfect metric. It does not measure the quality of the tests, only the amount of code covered by the tests. Code coverage of 100% does not mean that tests are effective or that they cover all possible cases. 

To run tests with coverage, use the following command:

```bash
pip install coverage
coverage run manage.py test
```

To view the results, you can either print them in the console, or generate an HTML report, which you can then find under `htmlcov/index.html`.

```bash
# print report
coverage report

# generate HTML report
coverage html
```

### 5. Versions - Semantic Release

[Semantic Release](https://semantic-release.gitbook.io/semantic-release/) automates project versioning and release by analyzing commit messages. It uses semantic versioning conventions to mark development milestones and automatically determine application versions. Development milestones will inform the changelog and versions will lead to releases.

### 6. Packaging - Semantic Release

[Semantic Release](https://semantic-release.gitbook.io/semantic-release/) will then create a python package using the version number generated in the previous step. Once generated, the package is sent to one of Madbot API's Gitlab registries.

### 7. Building and deploying a controlled environment - Docker

[Docker](https://www.docker.com/) is a lightweight virtualization platform for creating, deploying and running applications portably and efficiently using containers. Containers are isolated environments that contain all the elements required to run an application. They guarantee that the application will run in the same way on all operating systems, and simplify application deployment.

We use Docker to create container images for the API. These images are then used to deploy the API on a server. An image is created for each new version of the API.

## Client - CI stages

### 1. Code quality

#### Linter - ESLint

[ESLint](https://eslint.org/) is a linter for JavaScript that detects and corrects syntax errors, style problems and non-recommended coding practices. It is widely used in JavaScript projects to maintain code quality and ensure consistency between developers.

ESLint includes a number of plugins that enable additional rules to be added for popular frameworks and libraries. We use ESLint with the [Vue.js](https://eslint.vuejs.org/) plugin to detect errors in Vue.js code. We also use the [Typescript](https://typescript-eslint.io/) plugin to detect errors in TypeScript code.

```bash
yarn lint
```

We'll be following the rules recommended by ESLint and the Vue.js and Typescript plugins. In addition to these, we will follow the following rules:

- [no-var](https://eslint.org/docs/latest/rules/no-var) - Require `let` or `const` instead of `var` (error)
- [prefer-const](https://eslint.org/docs/latest/rules/prefer-const) - Require `const` declarations for variables that are never reassigned after declared (error)
- [camelcase](https://eslint.org/docs/latest/rules/camelcase) - Enforce camelcase naming convention (warn)
- [eqeqeq](https://eslint.org/docs/latest/rules/eqeqeq) - Require the use of `===` and `!==` (error)
- [no-console](https://eslint.org/docs/latest/rules/no-console) - Disallow the use of `console` (warn)
- [no-alert](https://eslint.org/docs/latest/rules/no-alert) - Disallow the use of `alert`, `confirm`, and `prompt` (warn)

#### Formatter - Prettier

[Prettier](https://prettier.io/) is a code formatting tool that guarantees consistency in code formatting by automatically reorganizing it according to predefined rules. Prettier also integrates with ESLint, and we'll delegate the task of lining up code to it.

We'll use the recommended [prettierrc](https://json.schemastore.org/prettierrc) schema and the following rules:

- `printWidth: 100` - specify line length (Roughly tell Prettier the desired line length. Prettier will make both shorter and longer lines, but generally strive to meet the specified `printWidth`)
- `trailingComma: "es5"` - Insert trailing commas following the ES5 rules (objects, arrays, etc.) and in type parameters for TypeScript

```bash
yarn format
```

### 2. Building the application - Nuxi build

[Nuxi build](https://nuxt.com/docs/api/commands/build) is used to compile the project's source code and make it compatible with browsers. It also optimizes the code and makes it lighter.

```bash
yarn install
yarn build
```

### 3. Testing

Our CI also includes UI testing. These tests confirm that the final product corresponds to the needs of the end-users.

No tests are currently implemented for the Customer part of Madbot.

### 4. Versions - Semantic Release

[Semantic Release](https://semantic-release.gitbook.io/semantic-release/) automates project versioning and release by analyzing commit messages. It uses semantic versioning conventions to mark development milestones and automatically determine application versions. Development milestones will inform the changelog and versions will lead to releases.

### 5. Deployment - Docker

In the same way as for the API, we use Docker to create container images for the client. An image is created for each new version of the API.
