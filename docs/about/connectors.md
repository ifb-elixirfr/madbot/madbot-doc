# Our philosophy ? Connectors!

Its philosophy is to aggregate metadata around scientific projects by creating links, via connectors, to where the data is stored without storing the datasets themselves.

These connectors make Madbot highly flexible and adaptable to a wide range of research environments. It already interfaces with many data management and analysis tools used daily by researchers, such as electronic laboratory notebooks (Labguru, eLabFTW), file storage systems (NAS, Seafile), research data warehouses (Zenodo, very soon ENA), web-based analysis platforms (Omero, Galaxy) and cluster infrastructures accessible via SSH. The application's architecture is based on the ISA (Investigation, Study, Assay) standard, enabling connectors to be created with virtually all the tools.

## Data connector

Connectors for generating links (datalink) between data stored in a remote storage space and madbot.

## SSHFS

Connect to any NAS or cluster through SSH

[Read documentation](#){ .md-button }


## Galaxy

Galaxy is an open source, web-based platform for data intensive biomedical research.

[Read documentation](#){ .md-button }

## Submission connector

Connectors for submitting data to international thematic or general data bases.

## ENA

The European Nucleotide Archive (ENA) provides a comprehensive record of the world’s nucleotide sequencing information, covering raw sequencing data, sequence assembly information and functional annotation.

[Read documentation](#){ .md-button }

## Zenodo 

Zenodo is a general-purpose open repository developed under the European OpenAIRE program and operated by CERN

[Read documentation](#){ .md-button }