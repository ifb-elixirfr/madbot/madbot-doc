# Indicators

## Project activities

<div id="echart-container" style="width: 100%; height: 400px;"></div>

<script src="https://cdn.jsdelivr.net/npm/echarts/dist/echarts.min.js"></script>
<script>
    async function getCommitsPerMonthYear(projectId) {
        const commitsPerMonthYear = {};
        let page = 1;
        const perPage = 100;
        
        while (true) {
            const response = await fetch(`https://gitlab.com/api/v4/projects/ifb-elixirfr%2Fmadbot%2F${projectId}/repository/commits?per_page=${perPage}&page=${page}`);
            const commits = await response.json();
            
            if (commits.length === 0) break;
            
            commits.forEach(commit => {
            const date = new Date(commit.committed_date);
            const year = date.getFullYear();
            const month = date.getMonth() + 1;
            const key = `${year}-${month.toString().padStart(2, '0')}`;
            commitsPerMonthYear[key] = (commitsPerMonthYear[key] || 0) + 1;
            });
            
            page++;
        }
    
        return commitsPerMonthYear;
    }

    const container = document.getElementById('echart-container');
    const chart = echarts.init(container);
    chart.showLoading('default', {
        text: 'Loading data...',
        color: '#5470C6',
        textColor: '#000',
        maskColor: 'rgba(255, 255, 255, 0.8)',
        zlevel: 0
    });

    getCommitsPerMonthYear("madbot-api").then(commitsData => {
        const sortedData = Object.entries(commitsData).sort(([a], [b]) => a.localeCompare(b));
        const xAxisData = sortedData.map(([date]) => date);
        const seriesData = sortedData.map(([, count]) => count);

        const options = {
            title: {
            text: 'Commits per month - Madbot API'
            },
            tooltip: {},
            xAxis: {
            data: xAxisData
            },
            yAxis: {max:100},
            series: [
            {
                name: 'Exemple',
                type: 'bar',
                data: seriesData
            }
            ]
        };
        chart.hideLoading();;
        chart.setOption(options);
    });


</script>

<div id="plotly-container" style="width: 100%; height: 400px;">
  <div id="loader" style="
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    font-size: 16px;
    color: #666;
  ">
    Loading data...
  </div>
</div>

<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<script>

    async function getCommitsPerMonthYear(projectId) {
        const commitsPerMonthYear = {};
        let page = 1;
        const perPage = 100;
        
        while (true) {
            const response = await fetch(`https://gitlab.com/api/v4/projects/ifb-elixirfr%2Fmadbot%2F${projectId}/repository/commits?per_page=${perPage}&page=${page}`);
            const commits = await response.json();
            
            if (commits.length === 0) break;
            
            commits.forEach(commit => {
            const date = new Date(commit.committed_date);
            const year = date.getFullYear();
            const month = date.getMonth() + 1;
            const key = `${year}-${month.toString().padStart(2, '0')}`;
            commitsPerMonthYear[key] = (commitsPerMonthYear[key] || 0) + 1;
            });
            
            page++;
        }
    
        return commitsPerMonthYear;
    }

    getCommitsPerMonthYear("madbot-client").then(commitsData => {
        const sortedData = Object.entries(commitsData).sort(([a], [b]) => a.localeCompare(b));
        const xAxisData = sortedData.map(([date]) => date);
        const seriesData = sortedData.map(([, count]) => count);

        const data = [
            {
            x: xAxisData,
            y: seriesData,
            type: 'bar'
            }
        ];

        const layout = {
            title: 'Commits per month - Madbot Client',
            yaxis: {
                range: [0, 100]
            }
        };

        document.getElementById('loader').style.display = 'none';
        Plotly.newPlot('plotly-container', data, layout);
   });
</script>