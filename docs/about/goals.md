# Goals of Madbot

Madbot (Metadata And Data Brokering Online Tool) is a web application that provides a dashboard for managing research data and metadata. Its philosophy is to aggregate metadata around scientific projects by creating links, via connectors, to where the data is stored without storing the datasets themselves.
These connectors make Madbot highly flexible and adaptable to a wide range of research environments. It already interfaces with many data management and analysis tools used daily by researchers, such as electronic laboratory notebooks (Labguru, eLabFTW), file storage systems (NAS, Seafile), research data warehouses (Zenodo, very soon ENA), web-based analysis platforms (Omero, Galaxy) and cluster infrastructures accessible via SSH. The application's architecture is based on the ISA (Investigation, Study, Assay) standard, enabling connectors to be created with virtually all the tools.

Madbot offers a cross-disciplinary view of the data and metadata for each research project. It enables researchers and their teams to gradually adopt a virtuous approach to managing their data, while helping them to achieve FAIR publication.

All the developments and results of this project will be made available to the entire scientific community on the GitLab collaborative platform under an open licence.