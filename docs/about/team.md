# Team 

## Core team

### Current members

<div class="grid cards text-center" markdown>

-   ![](https://www.gravatar.com/avatar/a1d2c7d4d276fc7f05aa67e642394068?s=100){:.core-team-img } 

    [Thomas Denecker](mailto:thomas.denecker@france-bioinformatique.fr)

    Scrum Master

-   ![](https://www.gravatar.com/avatar/50ff07852c388c4dfe8446d1df5d06b8?s=100){:.core-team-img } 

    [Julien SEILER](mailto:julien.seiler@france-bioinformatique.fr)

    Scrum Master

-   ![](https://www.gravatar.com/avatar/cfbbc92afe2f37fd2c8b60a11402380b?s=100){:.core-team-img }  

    [Baptiste Rousseau](mailto:baptiste.rousseau@france-bioinformatique.fr)

    Full stack developer

-   ![](https://www.gravatar.com/avatar/9e75c569e66c333b76db9bda48146425?s=100){:.core-team-img }  

    [Imane Messak](mailto:imane.messak@france-bioinformatique.fr)

    Full stack developer

-   ![](https://www.gravatar.com/avatar/abbfcc93bb86ea5ff4f2ecc84250277d?s=100){:.core-team-img }  
    
    [Laurent Bouri](mailto:laurent.bouri@france-bioinformatique.fr)

    Full stack developer

</div>


### Former members


<div class="grid cards text-center" markdown>

-   ![](https://www.gravatar.com/avatar?s=100){:.core-team-img } 

    **Jaffar Gura**

    Intern

-   ![](https://www.gravatar.com/avatar?s=100){:.core-team-img } 

    **Anliat Mohamed**

    Data brokering

</div>


## Working group

The madbot project is progressing with the help of a very active working group. 

- Anne-Françoise Adam-Blondon
- Hélène Chiapello
- Guillaume Gay
- Nadia Goué
- Frédéric de Lamotte
- Marc Mongy
- Julien Paul
- Guillaume Seith
- Nicolas Torquet
- Oana Vigy


## Contributors


### API

<div id="user-list-api" class="grid cards collaborators"></div>

### Client

<div id="user-list-client" class="grid cards collaborators"></div>

### Documentation

<div id="user-list-doc" class="grid cards collaborators"></div>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"></script>
<script>
  document.addEventListener("DOMContentLoaded", function () {
    loadUsers('user-list-api', "madbot-api");
    loadUsers('user-list-client', "madbot-client");
    loadUsers('user-list-doc', "madbot-doc");
  });
</script>
