---
hide:
  - navigation

---

# Madbot

<div class="grid" markdown>
<figure markdown="span" >
 ![Image title](assets/madbot_logo.png){ align=left width="300" }
</figure>

<div markdown class="hero-text">
Madbot (Metadata And Data Brokering Online Tool) is a web application that provides a dashboard for managing research data and metadata. Its philosophy is to aggregate metadata around scientific projects by creating links, via connectors, to where the data is stored without storing the datasets themselves.

<div markdown style="text-align:center">
[:fontawesome-solid-paper-plane: Get started !](/Client/Basic_concepts/){ .md-button }
[:fontawesome-solid-right-to-bracket: Login](#){ .md-button }       
</div>
</div>


</div>
  

<div class="grid cards" markdown>

- :material-clock-fast:{ .lg .middle } __Set up in 5 minutes__

    ---

    Install [`madbot`](#) and get up
    and running in minutes

    [:octicons-arrow-right-24: Getting started](#)

- :fontawesome-solid-link:{ .lg .middle } __It's just Connectors__

    ---

    A simple way of interfacing with tools such as Galaxy, a Nas, etc.

    [:octicons-arrow-right-24: Read more](/about/connectors/)

- :fontawesome-solid-timeline:{ .lg .middle } __Made to reconcile with data management__

    ---

    Add data, describe it with simple metadata, publish it, etc.

    [:octicons-arrow-right-24: Discover the flow](#)

- :material-scale-balance:{ .lg .middle } __Open Source, BSD3__

    ---

    Madbot is licensed under the 3-Clause BSD License License and available on [GitLab](https://gitlab.com/ifb-elixirfr/madbot)

    [:octicons-arrow-right-24: License](https://opensource.org/license/bsd-3-clause/)

</div>

## More than a simple management tool

<div class="grid" markdown>
<div markdown>
### Metadata referential

We have set up a metadata referential to link the different metadata from our different connectors. This makes it possible to share metadata between the connectors and madbot in a standard way.

</div>
<div markdown style="text-align:center; padding:20px">
![Image title](assets/referential.png){ width="100%" }
</div>
</div>

<div class="grid" markdown>
<div markdown style="text-align:center; padding:20px">
![Image title](assets/databrokering.jpeg){ width="100%" }
</div>
<div markdown>
### Data brokering tool

We offer various submission connectors to automate the submission of your data to thematic (such as ENA) and generalist (such as zenodo) international databases.
</div>
</div>

<div class="grid" markdown>
<div markdown>
### More and more connectors

What's special about Madbot is that it makes it easy to integrate a wide range of connectors. We plan to implement new connectors very soon. The project is also open to collaboration on writing new ones, as is currently the case. 
</div>
<div markdown style="text-align:center; padding:20px">
![Image title](assets/new_connectors.png){ width="100%" }
</div>
</div>

## Our team

<div class="container-people">
    <div class="people">
        <img src="https://www.gravatar.com/avatar/a1d2c7d4d276fc7f05aa67e642394068?s=150" alt="Avatar">
        <h2>Thomas DENECKER</h2>
        <h3>Scrum Master</h3>
        <p><i class="fa-solid fa-location-dot"></i> IFB core - Paris</p>
        <div class="rs">
            <a href="https://gitlab.com/thomasdenecker"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M503.5 204.6L502.8 202.8L433.1 21C431.7 17.5 429.2 14.4 425.9 12.4C423.5 10.8 420.8 9.9 417.9 9.6C415 9.3 412.2 9.7 409.5 10.7C406.8 11.7 404.4 13.3 402.4 15.5C400.5 17.6 399.1 20.1 398.3 22.9L351.3 166.9H160.8L113.7 22.9C112.9 20.1 111.5 17.6 109.6 15.5C107.6 13.4 105.2 11.7 102.5 10.7C99.9 9.7 97 9.3 94.1 9.6C91.3 9.9 88.5 10.8 86.1 12.4C82.8 14.4 80.3 17.5 78.9 21L9.3 202.8L8.5 204.6C-1.5 230.8-2.7 259.6 5 286.6C12.8 313.5 29.1 337.3 51.5 354.2L51.7 354.4L52.3 354.8L158.3 434.3L210.9 474L242.9 498.2C246.6 500.1 251.2 502.5 255.9 502.5C260.6 502.5 265.2 500.1 268.9 498.2L300.9 474L353.5 434.3L460.2 354.4L460.5 354.1C482.9 337.2 499.2 313.5 506.1 286.6C514.7 259.6 513.5 230.8 503.5 204.6z"/></svg></a>
            <a href="https://orcid.org/0000-0003-1421-7641"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M294.8 188.2h-45.9V342h47.5c67.6 0 83.1-51.3 83.1-76.9 0-41.6-26.5-76.9-84.7-76.9zM256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm-80.8 360.8h-29.8v-207.5h29.8zm-14.9-231.1a19.6 19.6 0 1 1 19.6-19.6 19.6 19.6 0 0 1 -19.6 19.6zM300 369h-81V161.3h80.6c76.7 0 110.4 54.8 110.4 103.9C410 318.4 368.4 369 300 369z"/></svg></a>
        </div>
    </div>
    <div class="people">
        <img src="https://www.gravatar.com/avatar/50ff07852c388c4dfe8446d1df5d06b8?150" alt="Avatar">
        <h2>Julien SEILER</h2>
        <h3>Scrum Master</h3>
        <p><i class="fa-solid fa-location-dot"></i> IFB core - Strasbourg</p>
        <div class="rs">
            <a href="https://gitlab.com/julozi"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M503.5 204.6L502.8 202.8L433.1 21C431.7 17.5 429.2 14.4 425.9 12.4C423.5 10.8 420.8 9.9 417.9 9.6C415 9.3 412.2 9.7 409.5 10.7C406.8 11.7 404.4 13.3 402.4 15.5C400.5 17.6 399.1 20.1 398.3 22.9L351.3 166.9H160.8L113.7 22.9C112.9 20.1 111.5 17.6 109.6 15.5C107.6 13.4 105.2 11.7 102.5 10.7C99.9 9.7 97 9.3 94.1 9.6C91.3 9.9 88.5 10.8 86.1 12.4C82.8 14.4 80.3 17.5 78.9 21L9.3 202.8L8.5 204.6C-1.5 230.8-2.7 259.6 5 286.6C12.8 313.5 29.1 337.3 51.5 354.2L51.7 354.4L52.3 354.8L158.3 434.3L210.9 474L242.9 498.2C246.6 500.1 251.2 502.5 255.9 502.5C260.6 502.5 265.2 500.1 268.9 498.2L300.9 474L353.5 434.3L460.2 354.4L460.5 354.1C482.9 337.2 499.2 313.5 506.1 286.6C514.7 259.6 513.5 230.8 503.5 204.6z"/></svg></a>
            <a href="https://orcid.org/0000-0002-4549-5188"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M294.8 188.2h-45.9V342h47.5c67.6 0 83.1-51.3 83.1-76.9 0-41.6-26.5-76.9-84.7-76.9zM256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm-80.8 360.8h-29.8v-207.5h29.8zm-14.9-231.1a19.6 19.6 0 1 1 19.6-19.6 19.6 19.6 0 0 1 -19.6 19.6zM300 369h-81V161.3h80.6c76.7 0 110.4 54.8 110.4 103.9C410 318.4 368.4 369 300 369z"/></svg></a>
        </div>
    </div>
    <div class="people">
        <img src="https://www.gravatar.com/avatar/abbfcc93bb86ea5ff4f2ecc84250277d?s=150" alt="Avatar">
        <h2>Laurent BOURI</h2>
        <h3>Full stack developper</h3>
        <p><i class="fa-solid fa-location-dot"></i> IFB core - Strasbourg</p>
        <div class="rs">
            <a href="https://gitlab.com/lbouri1"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M503.5 204.6L502.8 202.8L433.1 21C431.7 17.5 429.2 14.4 425.9 12.4C423.5 10.8 420.8 9.9 417.9 9.6C415 9.3 412.2 9.7 409.5 10.7C406.8 11.7 404.4 13.3 402.4 15.5C400.5 17.6 399.1 20.1 398.3 22.9L351.3 166.9H160.8L113.7 22.9C112.9 20.1 111.5 17.6 109.6 15.5C107.6 13.4 105.2 11.7 102.5 10.7C99.9 9.7 97 9.3 94.1 9.6C91.3 9.9 88.5 10.8 86.1 12.4C82.8 14.4 80.3 17.5 78.9 21L9.3 202.8L8.5 204.6C-1.5 230.8-2.7 259.6 5 286.6C12.8 313.5 29.1 337.3 51.5 354.2L51.7 354.4L52.3 354.8L158.3 434.3L210.9 474L242.9 498.2C246.6 500.1 251.2 502.5 255.9 502.5C260.6 502.5 265.2 500.1 268.9 498.2L300.9 474L353.5 434.3L460.2 354.4L460.5 354.1C482.9 337.2 499.2 313.5 506.1 286.6C514.7 259.6 513.5 230.8 503.5 204.6z"/></svg></a>
            <a href="https://orcid.org/0000-0002-2297-1559"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M294.8 188.2h-45.9V342h47.5c67.6 0 83.1-51.3 83.1-76.9 0-41.6-26.5-76.9-84.7-76.9zM256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm-80.8 360.8h-29.8v-207.5h29.8zm-14.9-231.1a19.6 19.6 0 1 1 19.6-19.6 19.6 19.6 0 0 1 -19.6 19.6zM300 369h-81V161.3h80.6c76.7 0 110.4 54.8 110.4 103.9C410 318.4 368.4 369 300 369z"/></svg></a>
        </div>
    </div>
    <div class="people">
        <img src="https://www.gravatar.com/avatar/9e75c569e66c333b76db9bda48146425?s=150" alt="Avatar">
        <h2>Imane MESSAK</h2>
        <h3>Full stack developper</h3>
        <p><i class="fa-solid fa-location-dot"></i> IFB core - Paris</p>
        <div class="rs">
            <a href="https://gitlab.com/imanemessak"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M503.5 204.6L502.8 202.8L433.1 21C431.7 17.5 429.2 14.4 425.9 12.4C423.5 10.8 420.8 9.9 417.9 9.6C415 9.3 412.2 9.7 409.5 10.7C406.8 11.7 404.4 13.3 402.4 15.5C400.5 17.6 399.1 20.1 398.3 22.9L351.3 166.9H160.8L113.7 22.9C112.9 20.1 111.5 17.6 109.6 15.5C107.6 13.4 105.2 11.7 102.5 10.7C99.9 9.7 97 9.3 94.1 9.6C91.3 9.9 88.5 10.8 86.1 12.4C82.8 14.4 80.3 17.5 78.9 21L9.3 202.8L8.5 204.6C-1.5 230.8-2.7 259.6 5 286.6C12.8 313.5 29.1 337.3 51.5 354.2L51.7 354.4L52.3 354.8L158.3 434.3L210.9 474L242.9 498.2C246.6 500.1 251.2 502.5 255.9 502.5C260.6 502.5 265.2 500.1 268.9 498.2L300.9 474L353.5 434.3L460.2 354.4L460.5 354.1C482.9 337.2 499.2 313.5 506.1 286.6C514.7 259.6 513.5 230.8 503.5 204.6z"/></svg></a>
            <a href="https://orcid.org/0000-0002-1654-6652"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M294.8 188.2h-45.9V342h47.5c67.6 0 83.1-51.3 83.1-76.9 0-41.6-26.5-76.9-84.7-76.9zM256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm-80.8 360.8h-29.8v-207.5h29.8zm-14.9-231.1a19.6 19.6 0 1 1 19.6-19.6 19.6 19.6 0 0 1 -19.6 19.6zM300 369h-81V161.3h80.6c76.7 0 110.4 54.8 110.4 103.9C410 318.4 368.4 369 300 369z"/></svg></a>
        </div>
    </div>
    <div class="people">
        <img src="https://www.gravatar.com/avatar/cfbbc92afe2f37fd2c8b60a11402380b?s=150" alt="Avatar">
        <h2>Baptiste ROUSSEAU</h2>
        <h3>Full stack developper</h3>
        <p><i class="fa-solid fa-location-dot"></i> IFB core - Paris</p>
        <div class="rs">
            <a href="https://gitlab.com/nobabar"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M503.5 204.6L502.8 202.8L433.1 21C431.7 17.5 429.2 14.4 425.9 12.4C423.5 10.8 420.8 9.9 417.9 9.6C415 9.3 412.2 9.7 409.5 10.7C406.8 11.7 404.4 13.3 402.4 15.5C400.5 17.6 399.1 20.1 398.3 22.9L351.3 166.9H160.8L113.7 22.9C112.9 20.1 111.5 17.6 109.6 15.5C107.6 13.4 105.2 11.7 102.5 10.7C99.9 9.7 97 9.3 94.1 9.6C91.3 9.9 88.5 10.8 86.1 12.4C82.8 14.4 80.3 17.5 78.9 21L9.3 202.8L8.5 204.6C-1.5 230.8-2.7 259.6 5 286.6C12.8 313.5 29.1 337.3 51.5 354.2L51.7 354.4L52.3 354.8L158.3 434.3L210.9 474L242.9 498.2C246.6 500.1 251.2 502.5 255.9 502.5C260.6 502.5 265.2 500.1 268.9 498.2L300.9 474L353.5 434.3L460.2 354.4L460.5 354.1C482.9 337.2 499.2 313.5 506.1 286.6C514.7 259.6 513.5 230.8 503.5 204.6z"/></svg></a>
            <a href="https://orcid.org/0009-0002-1723-2732"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M294.8 188.2h-45.9V342h47.5c67.6 0 83.1-51.3 83.1-76.9 0-41.6-26.5-76.9-84.7-76.9zM256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm-80.8 360.8h-29.8v-207.5h29.8zm-14.9-231.1a19.6 19.6 0 1 1 19.6-19.6 19.6 19.6 0 0 1 -19.6 19.6zM300 369h-81V161.3h80.6c76.7 0 110.4 54.8 110.4 103.9C410 318.4 368.4 369 300 369z"/></svg></a>
        </div>
    </div>
</div>

## For badge fans (like us)

### API

![back](https://img.shields.io/badge/back-django-green)
![test](https://img.shields.io/badge/test-passing-green)

### Client

![front](https://img.shields.io/badge/front-nuxt-green)
![test](https://img.shields.io/badge/test-passing-green)

### Documentation

[![Built with Material for MkDocs](https://img.shields.io/badge/Material_for_MkDocs-526CFE?style=for-the-badge&logo=MaterialForMkDocs&logoColor=white)](https://squidfunk.github.io/mkdocs-material/)
![test](https://img.shields.io/badge/test-passing-green)

## Licence

All the developments and results of this project will be made available to the entire scientific community on the GitLab collaborative platform under an open licence.

[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Madbot (API & Client) is licensed under a [The 3-Clause BSD License License](https://opensource.org/license/bsd-3-clause/).

## Citation

If you use Matbot API project, please cite us :

IFB-ElixirFr, Matbot API, (2024), GitLab repository, <https://gitlab.com/ifb-elixirfr/madbot>

## Join ou community ! 

Would you like to create a connector for your favourite tool? Would you like to take part in development? Would you like us to come and present our tool to your group?

<div markdown style="text-align:center">
[:fontawesome-solid-envelope: Contact us!](#){ .md-button }    
</div>