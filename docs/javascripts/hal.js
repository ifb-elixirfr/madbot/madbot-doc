const API_URL = "https://api.archives-ouvertes.fr/search/?q=madbot&fl=fileMain_s,uri_s,label_s";
const container = document.getElementById("publications-container");

async function fetchPublications() {
    try {
    const response = await fetch(API_URL);
    const data = await response.json();

    const publications = data.response.docs;

    if (publications.length > 0) {
        publications.forEach((pub) => {

        const publicationDiv = document.createElement("div");
        publicationDiv.classList.add("publication");

        const title = document.createElement("h3");
        title.innerHTML = `<a href="${pub.uri_s}" target="_blank">${pub.label_s}</a>`;
        publicationDiv.appendChild(title);

        const iframe = document.createElement("iframe");
        iframe.src = pub.fileMain_s;
        iframe.classList.add("publication-iframe");
        publicationDiv.appendChild(iframe);

        container.appendChild(publicationDiv);
        });
    } else {
        container.innerHTML = "<p>No publications found.</p>";
    }
    } catch (error) {
    console.error("Error loading publications :", error);
    container.innerHTML = "<p>An error occurred when loading publications.</p>";
    }
}
fetchPublications();