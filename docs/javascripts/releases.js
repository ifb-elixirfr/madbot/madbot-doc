function convertReleaseMarkdownToHTML(markdownString) {
    const regex = /\[\s*([^\]]+)\s*\]\(([^)]+)\)\s+\(([^)]+)\)/;
  
    const match = markdownString.match(regex);
  
    if (!match) {
      console.error("error format");
      return "";
    }

    const version = match[1];
    const url = match[2]; 
    const date = match[3]; 
  
    return `
    <a href="${url}" target="_blank" rel="noopener noreferrer">${version}</a> (${date})
    `;
  }

function fetchGitLabReleases(targetDivId, repo) {
    const apiUrl = `https://gitlab.com/api/v4/projects/ifb-elixirfr%2Fmadbot%2F${repo}/releases`;
    console.log(apiUrl);
    const container = document.getElementById(targetDivId);
  
    if (!container) {
      console.error(`Div with ID "${targetDivId}" not found.`);
      return;
    }
  
    container.innerHTML = "Release loading...";
  
    fetch(apiUrl)
      .then(response => {
        if (!response.ok) {
          throw new Error(`Erreur HTTP ${response.status}: Impossible de récupérer les releases.`);
        }
        return response.json();
      })
      .then(releases => {
        if (!releases.length) {
          container.innerHTML = "Aucune release trouvée.";
          return;
        }
  
        // Générer le tableau des releases
        let table = `
          <table style="border-collapse: collapse;">
            <thead>
              <tr>
                <th style="padding: 12px;">Version</th>
                <th style="padding: 12px;">Date</th>
                <th style="padding: 12px;">Description</th>
              </tr>
            </thead>
            <tbody>
        `;
  
        releases.forEach(release => {
          const version = release.tag_name || "N/A";
          const date = release.released_at ? release.released_at.split("T")[0] : "N/A";
          const description = release.description ? release.description.split("\n")[0] : "N/A";
  
          table += `
            <tr>
              <td style="padding: 12px;">${version}</td>
              <td style="padding: 12px;">${date}</td>
              <td style="padding: 12px;">${convertReleaseMarkdownToHTML(description)}</td>
            </tr>
          `;
        });
  
        table += `
            </tbody>
          </table>
        `;
  
        // Insérer le tableau dans le conteneur
        container.innerHTML = table;
      })
      .catch(error => {
        console.error(error);
        container.innerHTML = `<p style="color: red;">Erreur : Impossible to load releases.</p>`;
      });
  }
  