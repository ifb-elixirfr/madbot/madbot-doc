
function createUserCard(name, email, role) {
    const hashedEmail = CryptoJS.SHA256( email );
    const gravatarUrl = `https://www.gravatar.com/avatar/${hashedEmail}?s=80&d=identicon`;

    return `
        <li>
        <img alt="" src="${gravatarUrl}"> 
        <a href="mailto:anliatmohamed@gmail.com">${name}</a>
        </li>
    `;
  }



  function loadUsers(containerId, repo) {
    const userListContainer = document.getElementById(containerId);
    if (!userListContainer) {
      console.error(`No div with 'ID "${containerId}" found.`);
      return;
    }
    members_url = (
        "https://gitlab.com/api/v4/projects/ifb-elixirfr%2Fmadbot%2F"
        + repo
        + "/repository/contributors"
    )

    axios.get(members_url) 
      .then(response => {
        const users = response.data; 
 
        users.forEach(user => {
          const userCard = createUserCard(user.name, user.email, user.role);
          userListContainer.innerHTML += userCard;
        });

 
      })
      .catch(error => {
        console.error('Error in users loading :', error);
        userListContainer.innerHTML = '<p>Ompossible to load users.</p>';
      });
    
  }
  