# Personal data policy on Madbot website

The CNRS UAR 3601 undertakes to ensure that the processing of personal data carried out their websites comply with the european General Data Protection Reglementation (GDPR) and the French law : « Loi Informatique et Libertés ».

The following personal data  :
- Account for site edit : First name – Last name – email
is likely to be collected by :

IFB – Institut Français de Bioinformatique
CNRS UAR 3601 – IFB-Core, Génoscope – 2 rue Gaston Crémieux, 91057 – ÉVRY – CEDEX

and is Data Protection Officer :

17 rue Notre Dames des Pauvres – 54519 Vandoeuvre lès Nancy Cedex, dpd.demandes@cnrs.fr

This data is only accessible to authorized users and retained only to account will be closed by user.

*No data transfer outside the European Union is carried out.*

If information is compulsory in order to access specific features of the site, this compulsory nature is indicated at the time the data is collected. If you refuse to provide mandatory information, you may not be able to access certain services.
In compliance with the applicable legal and regulatory provisions, in particular Law No. 78-17 of 6 January amended relating to data processing, files and freedoms and European Regulation No. 2016/679/EU of 27 April 2016 (applicable since 25 May 2018), you have the following rights:

- Exercise your right of access, to know the personal data concerning you;
- Request the updating of your data, if it is inaccurate;
- Request portability or erasure of your data;
- Request the deletion of your account;
- Request the limitation of the processing of your data;
- Oppose, for legitimate reasons, the processing of your data;
- Withdraw your consent to the processing of your data.
- If you’re an authenticated contributor, update or delete your data by logging into your account and configuring its settings;

To exercise these rigths, you can send your request to :

Guichet de support et d’orientation
Bureau: LIRMM Bat5, 1/168 – 860 rue St Priest 34095 Montpellier cedex 5
contact@france-bioinformatique.fr

You can also contact the Data Protection Officer at the following address:

DPD
17 rue Notre Dame des Pauvres
4519 Vandoeuvre lès Nancy Cedex
dpd.demandes@cnrs.fr

If, after contacting us, you believe that your rights in the field of Information Technology and Liberties are not respected, you are entitled to file a claim online with the CNIL online or by post : 3 Place de Fontenoy – TSA 80715 – 75334 PARIS CEDEX 07.
