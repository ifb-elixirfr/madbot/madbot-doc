# Terms of use

## 1. Acceptance of Terms

By accessing and using this website/application, you agree to comply with and be bound by these Terms of Use. If you do not agree with any part of these terms, you must not use this website/application.

## 2. Changes to Terms

We reserve the right to update or modify these Terms of Use at any time without prior notice. Continued use of the website/application after any changes constitutes acceptance of those changes.

## 3. Intellectual Property

All content on this website/application is licensed under the Creative Commons Attribution-ShareAlike (CC BY-SA) license unless otherwise stated. Under this license:
- You are free to share and adapt the content for any purpose, even commercially.
- You must provide proper attribution to the original creator (as specified on this site).
- Any adaptations or derivative works must be distributed under the same license (CC BY-SA).

For full details, refer to the official Creative Commons license: https://creativecommons.org/licenses/by-sa/4.0/.

## 4. User Responsibilities

You agree to:
- Use the website/application for lawful purposes only.
- Not engage in any activity that may disrupt or damage the website/application.
- Provide accurate and truthful information when required.

## 5. Third-Party Links

This website/application may include links to third-party websites. We are not responsible for the content, terms, or policies of these external sites.

## 6. Disclaimer of Liability

The website/application is provided on an "as-is" and "as-available" basis. We make no warranties, expressed or implied, regarding the accuracy, reliability, or availability of the content. We are not liable for any damages resulting from the use of the website/application.

## 7. Termination of Access

We reserve the right to suspend or terminate access to this website/application for any reason, without prior notice.

## 8. Governing Law

These Terms of Use are governed by the laws of France. Any disputes arising under these terms shall be resolved exclusively in the courts of France.
