
## Journée "Cycle de vie / Gestion des données de la biologie"

:fontawesome-regular-calendar: 2024/04/04

:fontawesome-solid-location-pin: Sophia Antopolis - France


!!! quote Citation

    Julien Seiler, Thomas Denecker. Univ_CotedAzur. (2024, 4 avril). Journée "Cycle de vie / Gestion des données de la biologie" 07 - Madbot : A gateway to open scienc , in Journée "Cycle de vie / Gestion des données de la biologie". [Vidéo]. Canal-U. [https://doi.org/10.60527/p4g9-jv83](https://doi.org/10.60527/p4g9-jv83).

<div style="text-align:center">
    <iframe width="560" height="315" src="https://www.canal-u.tv/chaines/univcotedazur/embed/153615?t=0" allowfullscreen></iframe>
</div>