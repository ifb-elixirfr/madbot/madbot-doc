# Releases

## Madbot API

<div id="release-api"></div>

[Show more](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/-/releases){ .md-button }

## Madbot Client

<div id="release-client"></div>

[Show more](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/-/releases){ .md-button }


<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
  document.addEventListener("DOMContentLoaded", function () {
    fetchGitLabReleases('release-api', "madbot-api");
    fetchGitLabReleases('release-client', "madbot-client");
  });
</script>